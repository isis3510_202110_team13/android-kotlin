package com.petservices

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.petservices.data.entities.PetOwner
import com.petservices.data.entities.PetWalker
import com.petservices.data.models.Location
import com.petservices.data.services.PetOwnerService
import com.petservices.utils.InternetUtil
import com.petservices.utils.LoadingDialogFragment
import com.petservices.utils.RoomDB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.IOException
import java.lang.Exception
import java.util.*

class MainActivity : AppCompatActivity() {

    private val AUTH_REQUEST_CODE = 123;
    private val providers = listOf(
        AuthUI.IdpConfig.EmailBuilder().build()
    )
    lateinit var firebaseAuth: FirebaseAuth
    lateinit var listener: FirebaseAuth.AuthStateListener

    private lateinit var loadingDialog: LoadingDialogFragment

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    val PERMISSION_ID = 44

    val ownerLocation = MutableLiveData<Location?>(null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        InternetUtil.active.observe(this,{ value ->
            if(value == true){
                showLoadingDialog()
            } else if(value == false){
                hideLoadingDialog()
            }
        })
        InternetUtil.noInternetCount.observe(this,{value ->
            if(value > 0){
                showNoInternetDialog()
            }
        })
    }
    
    private fun signOut() {
        // [START auth_fui_signout]
        AuthUI.getInstance()
            .signOut(this)
            .addOnCompleteListener {
                // ...
            }
        // [END auth_fui_signout]
    }

    private fun delete() {
        // [START auth_fui_delete]
        AuthUI.getInstance()
            .delete(this)
            .addOnCompleteListener {
                // ...
            }
        // [END auth_fui_delete]
    }

    fun showLoadingDialog() {
        runOnUiThread {
            loadingDialog = LoadingDialogFragment.show(supportFragmentManager)
        }
    }

    fun hideLoadingDialog() {
        runOnUiThread {
            loadingDialog.dismiss()
        }
    }

    fun showNoInternetDialog() {
        Snackbar.make(
            findViewById(android.R.id.content)!!,
            getString(R.string.errorNetworkConnection),
            Snackbar.LENGTH_INDEFINITE
        ).setDuration(3500).show()
    }

    fun showTurnGPSMessage() {
        Toast.makeText(this, "Please turn on" + " your location...", Toast.LENGTH_LONG)
            .show()
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        // check if permissions are given
        if (checkPermissions()) {

            // check if location is enabled
            if (isLocationEnabled()) {

                // getting last location from FusedLocationClient object
                fusedLocationClient.lastLocation
                    .addOnCompleteListener(OnCompleteListener<android.location.Location?> { task ->
                        val location = task.result
                        if (location == null) {
                            requestNewLocationData()
                        } else {
                            val latitude = location?.latitude ?: 0.0
                            val longitude = location?.longitude ?: 0.0
                            ownerLocation.value = Location("Point", listOf(longitude, latitude))
                            var cityName = "Not Found"
                            val gcd = Geocoder(this, Locale.getDefault())
                            try {
                                val addresses: List<Address> = gcd.getFromLocation(
                                    latitude, longitude,
                                    10
                                )
                                for (adrs in addresses) {
                                    if (adrs != null) {
                                        val city = adrs.locality
                                        if (city != null && city != "") {
                                            cityName = city
                                            Log.i(
                                                "City",
                                                "------------------------------>${cityName}<-------------------------------"
                                            )
                                        }
                                    }
                                }
                            } catch (e: IOException) {
                                Log.e("Location", "Location error")
                            }
                        }
                    })
            } else {
                showTurnGPSMessage()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        // Initializing LocationRequest
        // object with appropriate methods
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 5
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        // setting LocationRequest on FusedLocationClient
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation = locationResult.lastLocation
            ownerLocation.value = Location(
                "Point",
                listOf(mLastLocation?.longitude ?: 0.0,mLastLocation?.latitude ?: 0.0)
            )
        }
    }

    // method to check for permissions
    private fun checkPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

        // If we want background location
        // on Android 10.0 and higher,
        // use:
        // ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    // method to request for permissions
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this, arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ), PERMISSION_ID
        )
    }

    // method to check
    // if location is enabled
    private fun isLocationEnabled(): Boolean {
        val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    // If everything is alright then
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(
            "Location",
            "------------------------------>Entreeeeee permissions result<-------------------------------"
        )
        if (requestCode == PERMISSION_ID) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation()
            }
        }
    }
}