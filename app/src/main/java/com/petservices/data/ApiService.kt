package com.petservices.data

import androidx.collection.ArrayMap
import com.petservices.data.models.*
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

private const val BASE_URL = "https://secure-island-73617.herokuapp.com/"

/**
 * Build the Moshi object that Retrofit will be using, making sure to add the Kotlin adapter for
 * full Kotlin compatibility.
 */
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

/**
 * Use the Retrofit builder to build a retrofit object using a Moshi converter with our Moshi
 * object.
 */
private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

/**
 * A public interface that exposes the [getWalks] method
 */
interface ApiService {

    @GET("petWalkers")
    suspend fun getDogWalkers(@Query(value = "petOwnerUid") userUid: String?, @Query(value = "long") longitude: Double?,@Query(value = "lat") latitude: Double?,@Query(value = "distance") distance: Int?, @Query(value = "orderBy") orderBy: String?, @Query(value = "limit") limit: Int?,  @Query(value = "matched") matched: Boolean?): List<DogWalker>

    @GET("petOwners/{userUid}/pets")
    suspend fun getPets(@Path("userUid") userUid:String): List<PetID>

    @POST("petOwners/{userUid}/pets")
    suspend fun addNewPet(@Path("userUid") userUid:String,@Body pet: Pet)

    @POST("petOwners")
    suspend fun addNewUser(@Body user: UID)

    @POST("petOwners/{uid}/panics")
    suspend fun panicButton(@Path("uid") uid:String)

    @GET("walks/{walkId}")
    suspend fun getWalk(@Path("walkId") walkId:Long):Walk

    @GET("walks/{walkId}")
    suspend fun getWalk2(@Path("walkId") walkId:Long):Walk

    @POST("walks")
    suspend fun createWalk(@Body walk: Walk)

    @PUT("walks/{walkId}")
    suspend fun updateWalk(@Path("walkId") walkId:Long,@Body newStatus:NewStatus)

    @PUT("walks/{walkId}")
    suspend fun updateWalk(@Path("walkId") walkId:Long,@Body newStatus:NewStatusRate)

    @GET("petOwners/{userUid}/walks")
    suspend fun getWalks(@Path("userUid") userUid: String?): List<Walk>

}
/**
 * A public Api object that exposes the lazy-initialized Retrofit service
 */
object Api {
    val retrofitService : ApiService by lazy { retrofit.create(ApiService::class.java) }
}