package com.petservices.data.DAOs;

import androidx.room.*
import com.petservices.data.entities.Pet

@Dao
interface PetDAO {
    @Query("Select * from Pet")
    suspend fun getAll(): List<Pet>

    @Query("Select * from Pet where id=:id")
    suspend fun getById(id: Long): Pet

    @Query("Select * from Pet where petOwnerId=:id")
    suspend fun getByOwnerId(id: Integer): Pet

    @Insert
    suspend fun insert(Pets: List<Pet>)

    @Update
    suspend fun update(Pet: Pet)

    @Delete
    suspend fun delete(Pet: Pet)

    @Query("DELETE FROM Pet")
    suspend fun deleteAll()
}