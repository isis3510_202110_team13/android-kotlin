package com.petservices.data.DAOs;

import androidx.room.*
import com.petservices.data.entities.PetOwner

@Dao
interface PetOwnerDAO {
    @Query("Select * from PetOwner")
    suspend fun getAll(): List<PetOwner>

    @Query("Select * from PetOwner where id=:id")
    suspend fun getById(id: Integer): PetOwner

    @Insert
    suspend fun insert(PetOwner: PetOwner)

    @Update
    suspend fun update(PetOwner: PetOwner)

    @Delete
    suspend fun delete(PetOwner: PetOwner)

    @Query("DELETE FROM PetOwner")
    suspend fun deleteAll()
}