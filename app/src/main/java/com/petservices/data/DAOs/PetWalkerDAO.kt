package com.petservices.data.DAOs;

import androidx.room.*
import com.petservices.data.entities.PetWalker

@Dao
interface PetWalkerDAO {
    @Query("Select * from PetWalker")
    suspend fun getAll(): List<PetWalker>

    @Query("Select * from PetWalker where distance < :distance")
    suspend fun getAllByDistance(distance:Int): List<PetWalker>

    @Query("Select * from PetWalker limit 1")
    fun getOne(): List<PetWalker>

    @Query("Select * from PetWalker where id=:id")
    suspend fun getById(id: String): PetWalker

    @Query("Select * from PetWalker order by avgRating desc limit 10")
    suspend fun getTopRated(): List<PetWalker>

    @Query("Select * from PetWalker where distance < :distance order by avgRating desc limit 10")
    suspend fun getTopRatedByDistance(distance: Int): List<PetWalker>

    @Query("Select * from PetWalker order by distance asc")
    suspend fun getNear(): List<PetWalker>

    @Query("Select * from PetWalker where distance < :distance order by distance asc")
    suspend fun getNearByDistance(distance: Int): List<PetWalker>

    @Insert
    suspend fun insert(PetWalkers: List<PetWalker>)

    @Update
    suspend fun update(PetWalker: PetWalker)

    @Delete
    suspend fun delete(PetWalker: PetWalker)

    @Query("DELETE FROM PetWalker")
    suspend fun deleteAll()

    @Query("DELETE FROM PetWalker where distance <= :distance")
    suspend fun deleteAllByDistance(distance:Int)
}