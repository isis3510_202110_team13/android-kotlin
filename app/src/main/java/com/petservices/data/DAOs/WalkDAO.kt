package com.petservices.data.DAOs;

import androidx.room.*

import com.petservices.data.entities.WalkE;

@Dao
interface WalkDAO {
    @Query("Select * from WalkE")
    suspend fun getAll(): List<WalkE>

    @Query("Select * from WalkE where id=:id")
    suspend fun getById(id:Int): WalkE

    @Insert
    suspend fun insert(walks:List<WalkE>)

    @Update
    suspend fun update(walk:WalkE)

    @Delete
    suspend fun delete(walk:WalkE)

    @Query("DELETE FROM WalkE")
    suspend fun deleteAll()
}