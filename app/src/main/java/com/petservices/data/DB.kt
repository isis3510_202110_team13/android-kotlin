package com.petservices.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.petservices.data.DAOs.PetWalkerDAO
import com.petservices.data.DAOs.WalkDAO
import com.petservices.data.DAOs.PetOwnerDAO
import com.petservices.data.DAOs.PetDAO
import com.petservices.data.entities.WalkE
import com.petservices.data.entities.PetWalker
import com.petservices.data.entities.PetOwner
import com.petservices.data.entities.Pet

@Database(
    entities = [WalkE::class, PetWalker::class, PetOwner::class, Pet::class],
    version=1
)
abstract class DB : RoomDatabase() {
    abstract fun walkDAO(): WalkDAO
    abstract fun petWalkerDAO(): PetWalkerDAO
    abstract fun petOwnerDAO(): PetOwnerDAO
    abstract fun petDAO(): PetDAO

    companion object {

        private var INSTANCE: DB? = null

        fun getInstance(context: Context): DB {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, DB::class.java, "roomDB.db").build()
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}