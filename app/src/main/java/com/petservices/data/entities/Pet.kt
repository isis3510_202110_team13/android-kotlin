package com.petservices.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(foreignKeys = arrayOf(ForeignKey(entity = PetOwner::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("petOwnerId"),
    onDelete = ForeignKey.CASCADE)))
data class Pet(
    @PrimaryKey(autoGenerate = false)
    val id: Long,
    val name: String?,
    val breed: String?,
    val age: Integer?,
    val size: String?,
    val photoUrl: String?,
    val petOwnerId: String,
    val Tags:String?
)