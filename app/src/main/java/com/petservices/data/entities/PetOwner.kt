package com.petservices.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PetOwner(
    @PrimaryKey(autoGenerate = false)
    val id:String,
    val displayName: String?,
    val city: String?,
    val photoUrl: String?,
    val latitude:Double?,
    val longitude:Double?
)