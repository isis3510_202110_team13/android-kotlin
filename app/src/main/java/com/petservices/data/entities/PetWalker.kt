package com.petservices.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PetWalker(
    @PrimaryKey(autoGenerate = false)
    val id:String,
    val available:Boolean?,
    val description:String?,
    val displayName:String?,
    val photoUrl:String?,
    val avgRating:Double?,
    val city:String?,
    val latitude:Double?,
    val longitude:Double?,
    val distance:Double?
)