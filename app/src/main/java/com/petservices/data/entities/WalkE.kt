package com.petservices.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(foreignKeys = arrayOf(
    ForeignKey(entity = Pet::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("petId"),
        onDelete = ForeignKey.CASCADE),
    ForeignKey(entity = PetWalker::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("petWalkerId"),
        onDelete = ForeignKey.CASCADE)))
data class WalkE(
    @PrimaryKey(autoGenerate = true)
    val id:Long,
    val startTime:String?,
    val endTime:String?,
    val status:String?,
    val rating:Double?,
    val petId:Long?,
    val petWalkerId: String?
)