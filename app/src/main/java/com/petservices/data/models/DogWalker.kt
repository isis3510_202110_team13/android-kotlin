package com.petservices.data.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue


@Parcelize
data class DogWalker(
    val uid: String,
    val displayName: String? = "",
    val photoURL: String? = "",
    val description: String? = "",
    val avgRating: Double? = 0.0,
    val currentLocation: @RawValue FullLocation? = null,
    val available: Boolean? = false,
    val city: String? = "",
    val distance: Double? = null,
    val Tags: @RawValue List<Tag>? = null
) : Parcelable