package com.petservices.data.models

data class FullLocation(val crs:CRS? = null,val type:String? = "",val coordinates:List<Double>)

data class Location(val type:String? = "", val coordinates:List<Double>? = null)
data class CRS(val type:String? = "", val properties:Properties? = null)
data class Properties(val name:String? = "")
