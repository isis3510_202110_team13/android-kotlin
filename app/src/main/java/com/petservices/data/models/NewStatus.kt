package com.petservices.data.models

data class NewStatus(val status: String)
