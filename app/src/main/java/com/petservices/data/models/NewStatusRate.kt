package com.petservices.data.models

data class NewStatusRate(val status: String,val rating: Double)
