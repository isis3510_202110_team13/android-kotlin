package com.petservices.data.models

data class Pet(val name:String?, val breed:String?, val age:Int?, val size: String?, val photoURL:String?, val Tags:List<Tag>?)
