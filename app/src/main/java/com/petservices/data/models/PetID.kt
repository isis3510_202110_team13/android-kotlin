package com.petservices.data.models

data class PetID(
    val id: Long?,
    val name: String? = "",
    val breed: String? = "",
    val age: Int? = null,
    val size: String? = "",
    val photoURL: String? = "",
    val PetOwnerId: String?
)
