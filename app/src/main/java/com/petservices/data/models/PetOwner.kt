package com.petservices.data.models

import android.net.Uri

data class PetOwner(
    val uid: String? = "",
    val displayName: String? = "",
    val photoURL: String? = "",
    val currentLocation:Location? = null
)