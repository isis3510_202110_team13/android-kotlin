package com.petservices.data.models

data class PetWithOwner(val name:String? = "", val breed:String? = "", val age:Int? = 0, val size: String? = "", val photoURL:String? = "", val PetOwnerUid:String? = "")
