package com.petservices.data.models

data class Tag(val name: String)
