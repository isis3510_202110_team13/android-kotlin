package com.petservices.data.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

@Parcelize
data class Walk(
    val id: Long? = null,
    var startTime: String? = null,
    var endTime: String? = null,
    val status: String? = "",
    val rating: Double? = 0.0,
    val createdAt: String? = "",
    val updatedAt: String? = "",
    val PetId: Long = 0,
    val PetWalkerUid: String="",
    val PetWalker: DogWalker? = null,
    val Pet: @RawValue PetID?
) : Parcelable
