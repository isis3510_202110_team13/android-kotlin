package com.petservices.data.services

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.petservices.data.Api
import com.petservices.data.models.*
import com.petservices.utils.RoomDB
import retrofit2.Response

object PetOwnerService {

    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance();

    suspend fun createPetOwner(uid: String){
        Api.retrofitService.addNewUser(UID(uid))
    }

    suspend fun createPet(uid:String, name:String,breed:String, age:Int, size: String, photo:String, tags: List<String>) {
        val petRegister = Pet(name, breed, age, size, photo, tags.map { Tag(it) })
        Api.retrofitService.addNewPet(uid,petRegister)
    }

    suspend fun getPets(): List<PetID>{
        val userUid = firebaseAuth.currentUser.uid
        return Api.retrofitService.getPets(userUid)
    }
}
