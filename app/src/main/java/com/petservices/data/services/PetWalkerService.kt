package com.petservices.data.services

import com.google.firebase.auth.FirebaseAuth
import com.petservices.data.Api
import com.petservices.data.models.*

object PetWalkerService {

    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance();

    suspend fun getPetWalkers(longitude: Double? = null, latitude: Double? = null,distance: Int? = null, limit: Int? = null, matched: Boolean = false): List<DogWalker> {
        val userUid = firebaseAuth.currentUser.uid
        return Api.retrofitService.getDogWalkers(userUid,longitude,latitude, distance,null,limit, matched)
    }

    suspend fun getNearPetWalkers(longitude: Double? = null, latitude: Double? = null, distance: Int? = null, limit: Int? = null, matched: Boolean = false): List<DogWalker> {
        val userUid = firebaseAuth.currentUser.uid
        return Api.retrofitService.getDogWalkers(userUid,longitude,latitude, distance,"distance",limit, matched)
    }

    suspend fun getTopRatedPetWalkers(longitude: Double? = null, latitude: Double? = null, distance: Int? = null, limit: Int? = null, matched: Boolean = false): List<DogWalker> {
        val userUid = firebaseAuth.currentUser.uid
        return Api.retrofitService.getDogWalkers(userUid,longitude,latitude, distance,"avgRating",limit, matched)
    }
}