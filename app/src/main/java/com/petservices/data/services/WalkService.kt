package com.petservices.data.services

import androidx.collection.ArrayMap
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import com.petservices.data.Api
import com.petservices.data.models.NewStatus
import com.petservices.data.models.NewStatusRate
import com.petservices.data.models.Walk

object WalkService {

    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    suspend fun updateWalkStatus(id:Long, newState:String) {
        Api.retrofitService.updateWalk(id, NewStatus(newState))
    }

    suspend fun updateWalkStatusRate(id:Long, newState:String, rating:Double) {
        Api.retrofitService.updateWalk(id, NewStatusRate(newState,rating))
    }

    suspend fun createWalk(walk: Walk){
        Api.retrofitService.createWalk(walk)
    }

    suspend fun getWalk(id:Long): Walk {
        return Api.retrofitService.getWalk(id)
    }

    suspend fun panicButton() {
        FirebaseAuth.getInstance().uid?.let { Api.retrofitService.panicButton(it) }
    }

    suspend fun getWalks(): List<Walk> {
        val userUid = firebaseAuth.currentUser.uid
        return Api.retrofitService.getWalks(userUid)
    }
}