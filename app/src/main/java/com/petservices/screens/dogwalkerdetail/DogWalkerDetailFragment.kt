package com.petservices.screens.dogwalkerdetail

import android.content.DialogInterface
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListAdapter
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.petservices.R
import com.petservices.data.models.Tag
import com.petservices.data.services.PetOwnerService
import com.petservices.databinding.DogWalkerDetailFragmentBinding
import com.petservices.screens.mainmenu.MainMenuFragmentDirections
import com.petservices.screens.petregister.PetRegisterFragmentDirections
import com.petservices.utils.InternetUtil
import kotlinx.coroutines.launch
import java.util.*

class DogWalkerDetailFragment : Fragment() {

    private lateinit var viewModel: DogWalkerDetailViewModel

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: DogWalkerDetailFragmentBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.dog_walker_detail_fragment,
            container,
            false
        )


        binding.lifecycleOwner = this

        val dogWalker = DogWalkerDetailFragmentArgs.fromBundle(requireArguments()).dogWalker
        val viewModelFactory = DogWalkerDetailViewModelFactoy(dogWalker)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(DogWalkerDetailViewModel::class.java)
        binding.viewModel = viewModel

        val errorObserver = Observer<Boolean?> { value ->
            if (value != null) {
                showError(getString(R.string.checkFields))
            }
        }
        val registerObserver = Observer<Boolean?> { value ->
            if (value != null) {
                showRegisterMessage(value)
            }
        }
        activity?.let { viewModel.showError.observe(it, errorObserver) }
        activity?.let { viewModel.registerSuccessful.observe(it, registerObserver) }

        val tags = viewModel.dogWalker.value!!.Tags?: listOf<Tag>()
        binding.dogWalkersTags.text = tags.map{ it.name }.joinToString(separator = " | ")

        val constraintsBuilder =
            CalendarConstraints.Builder()
                .setValidator(DateValidatorPointForward.now())

        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                .setTitleText("Select date")
                .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
                .setCalendarConstraints(constraintsBuilder.build())
                .build()

        datePicker.addOnPositiveButtonClickListener {
            val simpleFormat = SimpleDateFormat("yyyy-MM-dd")
            val date = Date(it)
            viewModel.setDate(simpleFormat.format(date))
        }

        binding.pickDate.setOnClickListener {
            datePicker.show(parentFragmentManager, datePicker.toString())
        }

        val startTimePicker =
            MaterialTimePicker.Builder()
                .setTimeFormat(TimeFormat.CLOCK_12H)
                .setHour(12)
                .setMinute(10)
                .setTitleText("Select appointment start time")
                .build()

        startTimePicker.addOnPositiveButtonClickListener {
            viewModel.setStartTime("${startTimePicker.hour}:${startTimePicker.minute}")
        }

        binding.pickStartTime.setOnClickListener {
            startTimePicker.show(parentFragmentManager, startTimePicker.toString())
        }


        val endTimePicker =
            MaterialTimePicker.Builder()
                .setTimeFormat(TimeFormat.CLOCK_12H)
                .setHour(12)
                .setMinute(10)
                .setTitleText("Select appointment end time")
                .build()

        endTimePicker.addOnPositiveButtonClickListener {
            viewModel.setEndTime("${endTimePicker.hour}:${endTimePicker.minute}")
        }
        binding.pickEndTime.setOnClickListener {
            endTimePicker.show(parentFragmentManager, endTimePicker.toString())
        }

        viewModel.pets.observe(viewLifecycleOwner, { pets ->
            if (pets != null && pets.isNotEmpty()){
                val petNames = pets.map { it.name }.toList()
                val adapter =
                    ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, petNames)
                binding.petDropdown.setAdapter(adapter)
            }
        })

        binding.petDropdown.onItemClickListener =
            AdapterView.OnItemClickListener { p0, p1, p2, p3 ->
                viewModel.selectedPetId = viewModel.pets.value!![p2].id
            }

        binding.hireButton.setOnClickListener {
            if (InternetUtil.isConnectedOrShowMessage()) {
                viewModel.hire()
            }
        }


        return binding.root
    }

    private fun showError(message: String) {
        Snackbar.make(
            requireActivity().findViewById(android.R.id.content),
            message,
            Snackbar.LENGTH_LONG
        ).setDuration(3500).show()
    }

    private fun showRegisterMessage(success: Boolean) {
        val alertDialog: AlertDialog? = activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setPositiveButton("OK",
                    DialogInterface.OnClickListener { dialog, id ->
                        // User clicked OK button
                        val action = DogWalkerDetailFragmentDirections.actionDogWalkerDetailFragmentToWalksHistoryFragment()
                        findNavController().navigate(action)
                    })
            }
            // Create the AlertDialog
            builder.create()
        }
        if(success) {
            alertDialog?.setTitle(this.resources.getString(R.string.register_successful_title))
            alertDialog?.setMessage(this.resources.getString(R.string.register_successful_walk))
        }else{
            alertDialog?.setTitle(this.resources.getString(R.string.register_unsuccessful_title))
            alertDialog?.setMessage(this.resources.getString(R.string.register_unsuccessful_walk))
        }
        alertDialog?.show()
    }
}