package com.petservices.screens.dogwalkerdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.petservices.data.models.DogWalker
import com.petservices.data.models.PetID
import com.petservices.data.models.Walk
import com.petservices.data.services.PetOwnerService
import com.petservices.data.services.WalkService
import com.petservices.utils.InternetUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class DogWalkerDetailViewModel(dogWalker: DogWalker) : ViewModel() {

    private val hireVM = Job()
    private val uiScope = CoroutineScope(Dispatchers.IO + hireVM)

    private val _dogWalker = MutableLiveData<DogWalker>()
    val dogWalker: LiveData<DogWalker>
        get() = _dogWalker

    private val _pets = MutableLiveData<List<PetID>>()
    val pets : MutableLiveData<List<PetID>>
        get() = _pets

    var selectedPetId: Long? = null

    private var _selectedDate = MutableLiveData("Date")

    val selectedDate: LiveData<String>
        get() = _selectedDate

    val _selectedStartTime = MutableLiveData("Start Time")

    val selectedStartTime: LiveData<String>
        get() = _selectedStartTime

    private var _selectedEndTime = MutableLiveData("End Time")

    val selectedEndTime: LiveData<String>
        get() = _selectedEndTime

    val showError = MutableLiveData<Boolean?>(null)
    val registerSuccessful = MutableLiveData<Boolean?>(null)


    init {
        _dogWalker.value = dogWalker
        getPets()
    }
    private fun getPets(){
        if (InternetUtil.isConnectedOrShowMessage()) {
        viewModelScope.launch {
            _pets.value = PetOwnerService.getPets()
        }}
        else
        {
            _pets.value = emptyList()
        }
    }

    fun setDate(date: String){
        _selectedDate.value = date
    }

    fun setStartTime(startTime: String){
        _selectedStartTime.value = startTime
    }

    fun setEndTime(endTime: String){
            _selectedEndTime.value = endTime
    }

    private fun validate(): Boolean{
        return selectedDate.value!! != "Date" && selectedStartTime.value!! != "Start Time" && selectedEndTime.value!! != "End Time" && selectedPetId != null
    }

    fun hire(){
        if(validate()){
            uiScope.launch {
                val walk = Walk(
                    PetId = selectedPetId!!, PetWalkerUid = dogWalker.value!!.uid, startTime = "${selectedDate.value} ${selectedStartTime.value}", endTime = "${selectedDate.value} ${selectedEndTime.value}", rating = null, id = null, createdAt = null, Pet = null, PetWalker = _dogWalker.value,
                    status = null,
                    updatedAt = null)

                    WalkService.createWalk(walk)
            }
            registerSuccessful.value = true
        }else{
            if (showError.value == null) {
                showError.value = true
            } else {
                showError.value = !showError.value!!
            }
        }



    }

}