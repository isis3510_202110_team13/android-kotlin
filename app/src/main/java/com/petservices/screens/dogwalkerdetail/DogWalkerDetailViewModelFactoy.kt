package com.petservices.screens.dogwalkerdetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.petservices.data.models.DogWalker

class DogWalkerDetailViewModelFactoy(val dogWalker: DogWalker): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DogWalkerDetailViewModel(dogWalker) as T
    }
}