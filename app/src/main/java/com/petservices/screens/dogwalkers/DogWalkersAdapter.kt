package com.petservices.screens.dogwalkers

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.petservices.data.models.DogWalker
import com.petservices.databinding.DogWalkerItemBinding

class DogWalkersAdapter(private val onClickListener: OnClickListener): ListAdapter<DogWalker,DogWalkersAdapter.ViewHolder >(DogWalkerDiffCallback()) {
    
    class ViewHolder private constructor(private val binding: DogWalkerItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(item: DogWalker) {
            binding.dogWalker = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = DogWalkerItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(item)
        }
        holder.bind(item)
    }
}

class DogWalkerDiffCallback : DiffUtil.ItemCallback<DogWalker>() {

    override fun areItemsTheSame(oldItem: DogWalker, newItem: DogWalker): Boolean {
        return oldItem.displayName == newItem.displayName
    }


    override fun areContentsTheSame(oldItem: DogWalker, newItem: DogWalker): Boolean {
        return oldItem == newItem
    }
}

class OnClickListener(val clickListener: (dogWalker:DogWalker) -> Unit) {
    fun onClick(dogWalker:DogWalker) = clickListener(dogWalker)
}