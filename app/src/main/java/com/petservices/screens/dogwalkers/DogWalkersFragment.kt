package com.petservices.screens.dogwalkers

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat
import androidx.fragment.app.*
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.*
import com.petservices.MainActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.snackbar.Snackbar
import com.petservices.databinding.DogWalkersFragmentBinding
import com.petservices.R
import com.petservices.utils.InternetUtil
import java.io.IOException
import java.util.*

class DogWalkersFragment : Fragment() {

    private lateinit var binding: DogWalkersFragmentBinding
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    val PERMISSION_ID = 44
    private val viewModel: DogWalkersViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)

        binding = DogWalkersFragmentBinding.inflate(inflater)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        val adapter = DogWalkersAdapter(OnClickListener { this.findNavController().navigate(DogWalkersFragmentDirections.actionDogWalkersFragmentToDogWalkerDetailFragment(it)) })
        binding.dogWalkersList.adapter = adapter

        binding.sortChipGroup.setOnCheckedChangeListener { _, _ ->
            handleFilterChange()
        }

        binding.distanceChipGroup.setOnCheckedChangeListener { _, _ ->
            handleFilterChange()
        }
        binding.matchedSwitch.setOnCheckedChangeListener { _, _ ->
            handleFilterChange()
        }

        handleFilterChange()
        viewModel.dogWalkers.observe(viewLifecycleOwner, { dogWalkers ->
            if (dogWalkers == null || dogWalkers.isEmpty()){
                view?.let { Snackbar.make(
                    it,
                    getString(R.string.petWalkersEmpty),
                    Snackbar.LENGTH_SHORT
                ).show() }
            }
        })
        return binding.root
    }
    
    private fun handleFilterChange(){
        getLastLocation()
        viewModel.matched = binding.matchedSwitch.isChecked
        when (binding.distanceChipGroup.checkedChipId) {
            binding.chip100m.id -> {
                viewModel.distance = 100
            }
            binding.chip500m.id -> {
                viewModel.distance = 500
            }
            binding.chip1km.id -> {
                viewModel.distance = 1000
            }
            binding.chip5km.id -> {
                viewModel.distance = 5000
            }
            else -> {
                viewModel.distance = null
            }
        }
        when (binding.sortChipGroup.checkedChipId) {
            binding.topRatedChip.id -> {
                if (InternetUtil.isConnectedOrShowMessage()) {
                    if(InternetUtil.active.value == null || InternetUtil.active.value == false ) InternetUtil.active.value = true
                    viewModel.getTopRatedDogWalkers()
                }
                else {
                    viewModel.getStoredTopRatedDogWalkers()
                }
            }
            binding.closerChip.id -> {
                if (InternetUtil.isConnectedOrShowMessage()) {
                    if(InternetUtil.active.value == null || InternetUtil.active.value == false ) InternetUtil.active.value = true
                    viewModel.getNearDogWalkers()
                }
                else {
                    viewModel.getStoredNearDogWalkers()
                }
            }
            else -> {
                if (InternetUtil.isConnectedOrShowMessage()) {
                    if(InternetUtil.active.value == null || InternetUtil.active.value == false ) InternetUtil.active.value = true
                    viewModel.getPetWalkers()
                }
                else {
                    viewModel.getStoredDogWalkers()
                }
            }
        }

    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        // check if permissions are given
        if (checkPermissions()) {

            // check if location is enabled
            if (isLocationEnabled()) {

                // getting last location from FusedLocationClient object
                fusedLocationClient.lastLocation
                    .addOnCompleteListener(OnCompleteListener<Location?> { task ->
                        val location = task.result
                        if (location == null) {
                            requestNewLocationData()
                        } else {
                            val latitude = location?.latitude ?: 0.0
                            val longitude = location?.longitude ?: 0.0
                            viewModel.latitude = latitude
                            viewModel.longitude = longitude
                            Log.i(
                                "Latitude",
                                "------------------------------>${latitude}<-------------------------------"
                            )
                            Log.i(
                                "Longitude",
                                "------------------------------>${longitude}<-------------------------------"
                            )
                            var cityName = "Not Found"
                            val gcd = Geocoder(context, Locale.getDefault())
                            try {
                                val addresses: List<Address> = gcd.getFromLocation(
                                    latitude, longitude,
                                    10
                                )
                                for (adrs in addresses) {
                                    if (adrs != null) {
                                        val city = adrs.locality
                                        if (city != null && city != "") {
                                            cityName = city
                                            Log.i(
                                                "City",
                                                "------------------------------>${cityName}<-------------------------------"
                                            )
                                        }
                                    }
                                }
                            } catch (e: IOException) {
                                Log.e("Location","Location error")
                            }
                        }
                    })
            } else {
                (activity as MainActivity).showTurnGPSMessage()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        // Initializing LocationRequest
        // object with appropriate methods
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 5
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        // setting LocationRequest on FusedLocationClient
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
        fusedLocationClient.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation = locationResult.lastLocation
            Log.i(
                "Latitude",
                "------------------------------>${mLastLocation?.latitude}<-------------------------------"
            )
            Log.i(
                "Longitude",
                "------------------------------>${mLastLocation?.longitude}<-------------------------------"
            )
        }
    }

    // method to check for permissions
    private fun checkPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

        // If we want background location
        // on Android 10.0 and higher,
        // use:
        // ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    // method to request for permissions
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(), arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ), PERMISSION_ID
        )
    }

    // method to check
    // if location is enabled
    private fun isLocationEnabled(): Boolean {
        val locationManager = activity?.getSystemService(LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    // If everything is alright then
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_ID) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation()
            }
        }
    }
}