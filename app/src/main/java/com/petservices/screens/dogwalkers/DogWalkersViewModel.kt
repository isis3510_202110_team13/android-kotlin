package com.petservices.screens.dogwalkers

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.petservices.data.entities.PetWalker
import com.petservices.data.models.CRS
import com.petservices.data.models.DogWalker
import com.petservices.data.models.FullLocation
import com.petservices.data.models.Properties
import com.petservices.data.services.PetWalkerService
import com.petservices.utils.InternetUtil
import com.petservices.utils.RoomDB
import kotlinx.coroutines.*

class DogWalkersViewModel : ViewModel() {

    private val dogWalkerVM = Job()

    private val uiScope = CoroutineScope(Dispatchers.IO + dogWalkerVM)

    var longitude: Double? = null

    var latitude: Double? = null

    var distance: Int? = 1000

    var matched: Boolean = false

    private val _dogWalkers = MutableLiveData<List<DogWalker>>()

    val dogWalkers: LiveData<List<DogWalker>>
        get() = _dogWalkers

    init {

    }

    fun getPetWalkers() {
            uiScope.launch {
                try {
                    var retrieved = PetWalkerService.getPetWalkers(longitude, latitude, distance, matched = matched)
                    Log.i(
                        "DogWalkersViewModel",
                        "Pet Walkers have been retrieved" + _dogWalkers.value.toString()
                    )
                    launch(Dispatchers.Main) {
                        _dogWalkers.value = retrieved
                    }
                    Log.i(
                        "DogWalkersViewModel",
                        "Pet Walkers have been collected" + _dogWalkers.value.toString()
                    )
                    var stored: MutableList<PetWalker> = ArrayList()
                    var pwDAO = RoomDB.getDB().petWalkerDAO()
                    val size = retrieved.size
                    var pw: PetWalker
                    var i = 0
                    var dw: DogWalker
                    while (i < size) {
                        dw = retrieved[i]
                        pw = PetWalker(dw.uid, dw.available, dw.description, dw.displayName,
                            dw.photoURL, dw.avgRating, dw.city,
                            dw.currentLocation?.coordinates?.get(0),
                            dw.currentLocation?.coordinates?.get(1), dw.distance
                        )
                        stored.add(pw)
                        i++
                    }
                    if(distance==null)
                    {
                        pwDAO.deleteAll()
                    }
                    else
                    {
                        pwDAO.deleteAllByDistance(distance!!)
                    }
                    Log.i(
                        "DogWalkersViewModel",
                        "Pet Walkers have been registered" + stored.toString()
                    )
                    pwDAO.insert(stored.toList())
                    Log.i(
                        "DogWalkersViewModel",
                        "Pet Walkers have been roomed" + pwDAO.getAll().toString()
                    )
                }
                catch (e: Exception) {
                    launch(Dispatchers.Main) {
                        _dogWalkers.value = ArrayList()
                    }
                    Log.e("DogWalkersViewModel", e.message ?: "")
                }
                launch(Dispatchers.Main) {
                    InternetUtil.active.value = false
                }
            }
    }

    fun getStoredDogWalkers() {
        uiScope.launch{
            try {
                var saved:List<PetWalker>
                if(distance==null)
                {
                    saved = RoomDB.getDB().petWalkerDAO().getAll()
                }
                else
                {
                    saved = RoomDB.getDB().petWalkerDAO().getAllByDistance(distance!!)
                }
                var dws:MutableList<DogWalker> = ArrayList()
                var pw: PetWalker
                var dw: DogWalker
                var i = 0
                var size = saved.size
                while(i<size)
                {
                    pw = saved[i]
                    dw = DogWalker(pw.id, pw.displayName, pw.photoUrl, pw.description, pw.avgRating,
                        FullLocation(CRS("name", Properties("EPSG:4326")),"Point",
                            listOf<Double>(pw.latitude!!.toDouble(),pw.longitude!!.toDouble())), pw.available, pw.city,
                            pw.distance)
                    dws.add(dw)
                    i++
                }
                launch(Dispatchers.Main) {
                    _dogWalkers.value = dws
                }
                Log.i("DogWalkersViewModel", "Pet Walkers have been collected" + _dogWalkers.value.toString())
            } catch (e: Exception) {
                launch(Dispatchers.Main) {
                    _dogWalkers.value = ArrayList()
                }
                Log.e("DogWalkersViewModel", e.message?:"")
            }
            //TODO definir si usaremos el icono de carga cuando se trae datos desde room en vez de internet
            //launch(Dispatchers.Main) {
            //    InternetUtil.active.value = false
            //}
        }
    }
    fun getNearDogWalkers(){
            uiScope.launch {
                try {
                    val nearPetWalkers = PetWalkerService.getNearPetWalkers(
                        longitude,
                        latitude,
                        distance,
                        matched = matched
                    )
                    launch(Dispatchers.Main) {
                        _dogWalkers.value = nearPetWalkers
                    }
                    Log.i(
                        "DogWalkersViewModel",
                        "Pet Walkers have been collected" + _dogWalkers.value.toString()
                    )
                } catch (e: Exception) {
                    launch(Dispatchers.Main) {
                        _dogWalkers.value = ArrayList()
                    }
                    Log.e("DogWalkersViewModel", e.message ?: "")
                }
                launch(Dispatchers.Main) {
                    InternetUtil.active.value = false
                }
            }
    }

    fun getTopRatedDogWalkers() {
            uiScope.launch {
                Log.d("DogWalkersFragment", "launch -> ${Thread.currentThread().name}")
                try {
                    val topRated = PetWalkerService.getTopRatedPetWalkers(
                        longitude,
                        latitude,
                        distance,
                        matched = matched
                    )
                    launch(Dispatchers.Main) {
                        _dogWalkers.value = topRated
                    }
                } catch (e: Exception) {
                    launch(Dispatchers.Main) {
                        _dogWalkers.value = ArrayList()
                    }
                }
                launch(Dispatchers.Main) {
                    InternetUtil.active.value = false
                }
            }
    }

    fun getStoredTopRatedDogWalkers() {
        uiScope.launch {
            try {
                var saved:List<PetWalker>
                if(distance==null)
                {
                    saved = RoomDB.getDB().petWalkerDAO().getTopRated()
                }
                else
                {
                    saved = RoomDB.getDB().petWalkerDAO().getTopRatedByDistance(distance!!)
                }
                var dws:MutableList<DogWalker> = ArrayList()
                var size = saved.size
                var pw:PetWalker
                var dw:DogWalker
                var i = 0
                while(i<size)
                {
                    pw = saved[i]
                    dw = DogWalker(pw.id, pw.displayName, pw.photoUrl, pw.description, pw.avgRating,
                        FullLocation(CRS("name", Properties("EPSG:4326")),"Point",
                            listOf<Double>(pw.latitude!!.toDouble(),pw.longitude!!.toDouble())), pw.available, pw.city,
                            pw.distance)
                    dws.add(dw)
                    i++
                }
                launch(Dispatchers.Main) {
                    _dogWalkers.value = dws
                }
                Log.i("DogWalkersViewModel", "Pet Walkers have been top rated" + _dogWalkers.value.toString())
            } catch (e: Exception) {
                launch(Dispatchers.Main) {
                    _dogWalkers.value = ArrayList()
                }
                Log.e("DogWalkersViewModel", e.message?:"")
            }
            launch(Dispatchers.Main) {
                InternetUtil.active.value = false
            }
        }
    }

    fun getStoredNearDogWalkers() {
        uiScope.launch {
            try {
                var saved:List<PetWalker>
                if(distance==null)
                {
                    saved = RoomDB.getDB().petWalkerDAO().getNear()
                }
                else
                {
                    saved = RoomDB.getDB().petWalkerDAO().getNearByDistance(distance!!)
                }
                var dws:MutableList<DogWalker> = ArrayList()
                var size = saved.size
                var pw:PetWalker
                var dw:DogWalker
                var i = 0
                while(i<size)
                {
                    pw = saved[i]
                    dw = DogWalker(pw.id, pw.displayName, pw.photoUrl, pw.description, pw.avgRating,
                        FullLocation(CRS("name", Properties("EPSG:4326")),"Point",
                            listOf<Double>(pw.latitude!!.toDouble(),pw.longitude!!.toDouble())), pw.available, pw.city,
                        pw.distance)
                    dws.add(dw)
                    i++
                }
                launch(Dispatchers.Main) {
                    _dogWalkers.value = dws
                }
                Log.i("DogWalkersViewModel", "Pet Walkers have been neared" + _dogWalkers.value.toString())
            } catch (e: Exception) {
                launch(Dispatchers.Main) {
                    _dogWalkers.value = ArrayList()
                }
                Log.e("DogWalkersViewModel", e.message?:"")
            }
            launch(Dispatchers.Main) {
                InternetUtil.active.value = false
            }
        }
    }
}