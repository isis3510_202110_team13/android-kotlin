package com.petservices.screens.login

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import com.petservices.R
import com.petservices.databinding.LogInFragmentBinding

class LogInFragment : Fragment() {

    private val viewModel: LogInViewModel by viewModels()
    private lateinit var sharedPref:SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPref = activity?.getSharedPreferences("data", Context.MODE_PRIVATE) ?: return
        val logged = sharedPref.getBoolean("registered", false)
        if (logged) {
            findNavController().popBackStack(R.id.logInFragment, true)
            findNavController().navigate(R.id.mainMenuFragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = LogInFragmentBinding.inflate(inflater)
        val errorObserver = Observer<Int> { value ->
            when (value) {
                1 -> showError(getString(R.string.check_email))
                2 -> showError(getString(R.string.check_password))
                3 -> goToHome()
            }
        }
        activity?.let { viewModel.messageType.observe(it, errorObserver) }
        activity?.let {
            viewModel.message.observe(it, { value ->
                if (value.isNotEmpty() && value.isNotBlank()) {
                    showError(value)
                }
            })
        }
        binding.viewModel = viewModel
        binding.createAccountTv.setOnClickListener {
            val action = LogInFragmentDirections.actionLogInFragmentToRegisterFragment()
            findNavController().navigate(action)
        }
        binding.lifecycleOwner = this
        return binding.root
    }

    private fun showError(message: String) {
        Snackbar.make(
            requireActivity().findViewById(android.R.id.content),
            message,
            Snackbar.LENGTH_LONG
        ).setDuration(3500).show()
    }

    private fun goToHome(){
        with(sharedPref.edit()) {
            putBoolean("registered", true)
            commit()
        }
        findNavController().popBackStack(R.id.logInFragment, true)
        findNavController().navigate(R.id.mainMenuFragment)
    }
}