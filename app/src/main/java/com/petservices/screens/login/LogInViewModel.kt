package com.petservices.screens.login

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.petservices.utils.InternetUtil

class LogInViewModel : ViewModel() {
    val email = MutableLiveData("")
    val password = MutableLiveData("")
    private val _messageType = MutableLiveData(0)
    private val _message = MutableLiveData("")

    val messageType: LiveData<Int>
        get() = _messageType

    val message: LiveData<String>
        get() = _message

    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    fun login(view: View) {
        if (InternetUtil.isConnectedOrShowMessage()) {
            val emailClean = email.value ?: ""
            val passClean = password.value ?: ""
            var valid = true
            if (emailClean.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailClean)
                    .matches()
            ) {
                _messageType.value = 1
                valid = false
            } else if (passClean.length < 7) {
                _messageType.value = 2
                valid = false
            }
            if (valid) {
                InternetUtil.active.value = true
                firebaseAuth.signInWithEmailAndPassword(emailClean, passClean)
                    .addOnSuccessListener {
                            InternetUtil.active.value = false
                            _messageType.value = 3
                    }
                    .addOnFailureListener { error ->
                        InternetUtil.active.value = false
                        _message.value = error.message
                    }
            }
        }
    }
}