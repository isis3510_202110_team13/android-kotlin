package com.petservices.screens.mainmenu

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.commit
import androidx.navigation.fragment.findNavController
import com.petservices.R
import com.petservices.databinding.MainMenuFragmentBinding
import com.petservices.utils.InternetUtil
import com.petservices.utils.RoomDB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MainMenuFragment : Fragment() {

    private val mainMenuVM = Job()

    private val uiScope = CoroutineScope(Dispatchers.IO + mainMenuVM)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: MainMenuFragmentBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.main_menu_fragment,
            container,
            false
        )

        binding.dogWalkersButton.setOnClickListener {
            if (InternetUtil.isConnected()) {
                val action = MainMenuFragmentDirections.actionMainMenuFragmentToDogWalkersFragment()
                findNavController().navigate(action)
            }
            else {
                uiScope.launch(Dispatchers.IO) {
                    var saved = RoomDB.getDB().petWalkerDAO().getOne()
                    Log.i("MainMenu", "There is a petwalker" + saved.toString())
                    if(saved.isNotEmpty()){
                        val action = MainMenuFragmentDirections.actionMainMenuFragmentToDogWalkersFragment()
                        findNavController().navigate(action)
                    }
                    else {
                        val action = MainMenuFragmentDirections.actionMainMenuFragmentToNoInternetFragment()
                        findNavController().navigate(action)
                    }
                }
            }
        }
        binding.petRegisterButton.setOnClickListener {
            val action = MainMenuFragmentDirections.actionMainMenuFragmentToPetRegisterFragment()
            findNavController().navigate(action)
        }
        binding.walksHistoryButton.setOnClickListener {
            val action = MainMenuFragmentDirections.actionMainMenuFragmentToWalksHistoryFragment()
            findNavController().navigate(action)
        }
        return binding.root
    }
}