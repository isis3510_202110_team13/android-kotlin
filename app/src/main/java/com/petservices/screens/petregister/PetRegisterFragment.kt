package com.petservices.screens.petregister

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.google.android.material.snackbar.Snackbar
import com.petservices.R
import com.petservices.databinding.FragmentPetRegisterBinding
import com.petservices.screens.mainmenu.MainMenuFragmentDirections

class PetRegisterFragment : Fragment() {

    private val viewModel: PetRegisterViewModel by viewModels()
    private val PHOTO_REQUEST_CODE = 2000

    lateinit var sizeTypes: Array<String>
    lateinit var tags: Array<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentPetRegisterBinding.inflate(inflater)
        val errorObserver = Observer<Boolean?> { value ->
            if (value != null) {
                showError(getString(R.string.checkFields))
            }
        }
        val registerObserver = Observer<Boolean?> { value ->
            if (value != null) {
                showRegisterMessage(value)
            }
        }
        activity?.let { viewModel.showError.observe(it, errorObserver) }
        activity?.let { viewModel.registerSuccessful.observe(it, registerObserver) }
        binding.petRegisterImageBtn.setOnClickListener { openCamera() }
        sizeTypes = resources.getStringArray(R.array.dogSizes)

        var adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, sizeTypes)
        binding.sizeDropdown.setAdapter(adapter)
        binding.sizeDropdown.onItemClickListener =
            AdapterView.OnItemClickListener {
                    p0, p1, p2, p3 -> viewModel.size.value = sizeTypes[p2]
            }

        tags = resources.getStringArray(R.array.tags)

        for(tag in tags){
            val chip = Chip(requireContext())
            chip.text = tag
            chip.isCheckable=true
            chip.isChecked = viewModel.tags.value!!.contains(tag)
            chip.setOnCheckedChangeListener { _, isChecked ->
                if(isChecked){
                    viewModel.tags.value?.add(tag)
                }else {
                    viewModel.tags.value?.remove(tag)
                }
            }
            binding.tagsChipsGroup.addView(chip)
        }
        binding.viewModel = viewModel
        return binding.root
    }

    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, PHOTO_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == PHOTO_REQUEST_CODE) {
            val extras = data?.extras
            var petBitmap = extras?.get("data") as Bitmap
            viewModel.image.value = petBitmap
        }
    }

    private fun showError(message: String) {
        Snackbar.make(
            requireActivity().findViewById(android.R.id.content),
            message,
            Snackbar.LENGTH_LONG
        ).setDuration(3500).show()
    }

    private fun showRegisterMessage(success: Boolean) {
        val alertDialog: AlertDialog? = activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setPositiveButton("OK",
                    DialogInterface.OnClickListener { dialog, id ->
                        // User clicked OK button
                        val action = PetRegisterFragmentDirections.actionPetRegisterFragmentToMainMenuFragment()
                        findNavController().navigate(action)
                    })
            }
            // Create the AlertDialog
            builder.create()
        }
        if(success) {
            alertDialog?.setTitle(this.resources.getString(R.string.register_successful_title))
            alertDialog?.setMessage(this.resources.getString(R.string.register_successful_pet))
        }else{
            alertDialog?.setTitle(this.resources.getString(R.string.register_unsuccessful_title))
            alertDialog?.setMessage(this.resources.getString(R.string.register_unsuccessful_pet))
        }
        alertDialog?.show()
    }
}