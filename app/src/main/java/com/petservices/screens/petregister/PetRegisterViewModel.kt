package com.petservices.screens.petregister

import android.graphics.Bitmap
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.petservices.data.Api
import com.petservices.data.models.Pet
import com.petservices.data.services.PetOwnerService
import com.petservices.utils.InternetUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.util.*


class PetRegisterViewModel : ViewModel() {

    private val petRegisterVM = Job()
    private val uiScope = CoroutineScope(Dispatchers.IO + petRegisterVM)

    val name = MutableLiveData("")
    val breed = MutableLiveData("")
    val age = MutableLiveData("")
    val image = MutableLiveData<Bitmap>()
    val showError = MutableLiveData<Boolean?>(null)
    val registerSuccessful = MutableLiveData<Boolean?>(null)
    val size = MutableLiveData("")
    val tags = MutableLiveData(mutableListOf<String>())

    fun onRegisterClicked(view: View) {
        val intAge = if (age.value == "") 0 else Integer.parseInt(age.value)
        if ((name.value?.length ?: 0 > 2) && (breed.value?.length ?: 0 > 2) && (intAge > 0) && (image.value != null) && (size.value != "")) {
            if (InternetUtil.isConnectedOrShowMessage()) {
                InternetUtil.active.value = true
                val storageRef = FirebaseStorage.getInstance().reference
                val fileName = "pets/${UUID.randomUUID()}.jpg"
                val petRef = storageRef.child(fileName)
                val baos = ByteArrayOutputStream()
                image.value!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val data = baos.toByteArray()
                var uploadTask = petRef.putBytes(data)
                uploadTask.addOnSuccessListener {
                    uiScope.launch {
                        var success = true
                        try {
                            petRegister(fileName, intAge)
                        } catch (e:Exception){
                            success = false
                        } finally {
                            launch(Dispatchers.Main) {
                                InternetUtil.active.value = false
                                registerSuccessful.value = success
                            }
                        }
                    }
                }
            }
            else
            {
                //TODO maybe register pet to RoomDB and then synchronize
            }
        } else {
            if (showError.value == null) {
                showError.value = true
            } else {
                showError.value = !showError.value!!
            }
            Log.i(
                "Register",
                "----------------------------------->InValid<-----------------------------------"
            )
        }
    }

    private suspend fun petRegister(fileName: String, intAge: Int) {
        try {
            FirebaseAuth.getInstance().uid?.let {
                PetOwnerService.createPet(
                    it,
                    name.value!!,
                    breed.value!!,
                    intAge,
                    size.value!!,
                    fileName,
                    tags.value!!
                )
            }
            Log.i("Saving pet", "-------------------------->Todo OK<-----------------------")
        } catch (e: Exception) {
            Log.e("Error saving pet", e.message ?: "Unknown error")
        }
    }
}