package com.petservices.screens.petwalk

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.petservices.R
import com.petservices.databinding.PetWalkFragmentBinding
import com.petservices.screens.dogwalkerdetail.DogWalkerDetailFragmentArgs
import com.petservices.screens.dogwalkerdetail.DogWalkerDetailViewModel
import com.petservices.screens.dogwalkerdetail.DogWalkerDetailViewModelFactoy
import com.petservices.utils.GlideApp


class PetWalkFragment : Fragment() {

    private lateinit var viewModel: PetWalkViewModel
    private lateinit var startStopBtn: Button
    private var callIntent: Intent? = null
    private var rateDialog: AlertDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = PetWalkFragmentBinding.inflate(inflater)
        val walk = PetWalkFragmentArgs.fromBundle(requireArguments()).walk
        val viewModelFactory = PetWalkViewModelFactory(walk)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(PetWalkViewModel::class.java)
        binding.viewModel = viewModel
        val urlObserver = Observer<String> { newUrl ->
            if (newUrl.isNotEmpty()) {
                try {
                    var imageRoute = newUrl
                    if (newUrl.contains("https")) {
                        var imageSplitted = newUrl.split(".jpg")
                        imageRoute = "pets/" + imageSplitted[0].split("%2F")[1] + ".jpg"
                    }
                    val storageReference = Firebase.storage.reference.child(imageRoute)
                    GlideApp.with(this)
                        .load(storageReference)
                        .centerCrop()
                        .placeholder(R.drawable.perrito)
                        .into(binding.petIV)
                } catch (e: Exception) {
                    Log.e("Firebase Image Error", "Unknown Image")
                }
            }
        }
        val showRateObserver = Observer<Boolean?> { newOption ->
            if (newOption == true) {
                showRate()
            } else if(newOption == false){
                hideRateDialog()
            }
        }
        val walkerUrlObserver = Observer<String> { newUrl ->
            if (newUrl.isNotEmpty()) {
                try {
                    Glide.with(this)
                        .load(newUrl)
                        .centerCrop()
                        .placeholder(R.drawable.avatar)
                        .into(binding.petWalkerIV)
                } catch (e: Exception) {
                    Log.e("Firebase Image Error", "Unknown Image")
                }
            }
        }
        val statusObserver = Observer<String> { newStatus ->
            when (newStatus) {
                "finished" -> {
                    binding.startStopBtn.visibility = View.GONE
                    binding.emergencyBtnsLayout.visibility = View.GONE
                    binding.emergencyCallBtn.visibility = View.GONE
                    binding.locationTV.visibility = View.GONE
                    binding.cancelBtn.visibility = View.GONE
                }
                "hired" -> {
                    binding.startStopBtn.text = getString(R.string.start)
                    binding.emergencyBtnsLayout.visibility = View.GONE
                    binding.emergencyCallBtn.visibility = View.GONE
                    binding.locationTV.visibility = View.GONE
                }
                "canceled" -> {
                    binding.startStopBtn.visibility = View.GONE
                    binding.emergencyBtnsLayout.visibility = View.GONE
                    binding.emergencyCallBtn.visibility = View.GONE
                    binding.locationTV.visibility = View.GONE
                    binding.cancelBtn.visibility = View.GONE
                }
                "started" -> {
                    binding.startStopBtn.text = getString(R.string.stop)
                    binding.emergencyBtnsLayout.visibility = View.VISIBLE
                    binding.emergencyCallBtn.visibility = View.VISIBLE
                    binding.cancelBtn.visibility = View.GONE
                }

            }
        }
        activity?.let { viewModel.showRate.observe(it, showRateObserver) }
        activity?.let { viewModel.petImageUrl.observe(it, urlObserver) }
        activity?.let { viewModel.walkerImageUrl.observe(it, walkerUrlObserver) }
        activity?.let { viewModel.status.observe(it, statusObserver) }

        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.emergencyCallBtn.setOnClickListener {
            viewModel.panicButton()
            callIntent = Intent(Intent.ACTION_DIAL)
            callIntent!!.data = Uri.parse("tel:${viewModel.emergencyNumber}")
            startActivity(callIntent)
        }
        binding.policeCallBtn.setOnClickListener {
            viewModel.panicButton()
            callIntent = Intent(Intent.ACTION_DIAL)
            callIntent!!.data = Uri.parse("tel:${viewModel.policeNumber}")
            startActivity(callIntent)
        }
        binding.insuranceBtn.setOnClickListener {
            viewModel.panicButton()
            callIntent = Intent(Intent.ACTION_DIAL)
            callIntent!!.data = Uri.parse("tel:${viewModel.insuranceNumber}")
            startActivity(callIntent)
        }
        startStopBtn = binding.startStopBtn
        return binding.root
    }

    private fun showRate() {
        rateDialog = activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            builder.setView(inflater.inflate(R.layout.dialog_rate_walker, null))
                .setPositiveButton("OK", null)
            // Create the AlertDialog
            builder.setCancelable(false)
            builder.create()
        }
        rateDialog?.setCanceledOnTouchOutside(false)
        rateDialog?.show()
        rateDialog?.let {
            val positiveButton = it.getButton(AlertDialog.BUTTON_POSITIVE)
            positiveButton.setOnClickListener { view ->
                val rateET = it.findViewById<EditText>(R.id.rateWalkerEt)
                try {
                    val rate = rateET?.text.toString().toDouble()
                    viewModel.rate.value = rate
                    viewModel.startStopWalk(null)
                }catch (e: Exception){
                    Toast.makeText(activity, getString(R.string.rate_check), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun hideRateDialog(){
        if(rateDialog != null) {
            rateDialog?.dismiss()
            rateDialog = null
        }
    }
}