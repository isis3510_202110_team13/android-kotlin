package com.petservices.screens.petwalk

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.petservices.data.Api
import com.petservices.data.models.Walk
import com.petservices.data.services.WalkService
import com.petservices.utils.InternetUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PetWalkViewModel(walk: Walk) : ViewModel() {
    val walk = walk
    val walkerName = MutableLiveData("")
    val petImageUrl = MutableLiveData("")
    val walkerImageUrl = MutableLiveData("")
    val walkerLocation = MutableLiveData("")
    val startTime = MutableLiveData("")
    val endTime = MutableLiveData("")
    val status = MutableLiveData("")
    var walkId: Long = walk.id!!
    val showRate = MutableLiveData(false)
    val rate = MutableLiveData(0.0)
    var emergencyNumber = -1
    var policeNumber = -1
    var insuranceNumber = -1

    private val walkVM = Job()

    private val uiScope = CoroutineScope(Dispatchers.IO + walkVM)

    init {
        policeNumber = 123
        getWalk()
    }

    private fun getWalk() {
        if (InternetUtil.isConnectedOrShowMessage()) {
            InternetUtil.active.value = true
            uiScope.launch {
                try {
                    val location = walk.PetWalker?.currentLocation?.coordinates
                    launch(Dispatchers.Main) {
                        walkerName.value = walk.PetWalker?.displayName ?: ""
                        petImageUrl.value = walk.Pet?.photoURL ?: ""
                        walkerLocation.value = "${location?.get(0) ?: ""},${location?.get(1) ?: ""}"
                        walkerImageUrl.value = walk.PetWalker?.photoURL ?: ""
                        startTime.value = "Start time: " + (walk.startTime ?: "D/A")
                        endTime.value = "End time: " + (walk.endTime ?: "D/A")
                        status.value = walk.status
                    }
                } catch (e: Exception) {
                    Log.e("GetWalk", e.message ?: "Unknown Exception")
                } finally {
                    launch(Dispatchers.Main) {
                        InternetUtil.active.value = false
                    }
                }
            }
        }
    }

    fun cancelWalk(view: View) {
        if (InternetUtil.isConnectedOrShowMessage()) {
            uiScope.launch {
                WalkService.updateWalkStatus(walkId, "canceled")
                launch(Dispatchers.Main) {
                    status.value = "canceled"
                }
            }
        }
    }

    fun startStopWalk(view: View?) {
        if (InternetUtil.isConnectedOrShowMessage()) {
            InternetUtil.active.value = true
            uiScope.launch {
                if (status.value == "hired") {
                    WalkService.updateWalkStatus(walkId, "started")
                    launch(Dispatchers.Main) {
                        status.value = "started"
                        InternetUtil.active.value = false
                    }
                } else if (status.value == "started") {
                    when (showRate.value) {
                        true -> {
                            WalkService.updateWalkStatusRate(walkId, "finished",rate.value ?: 0.0)
                            launch(Dispatchers.Main) {
                                status.value = "finished"
                                showRate.value = false
                                InternetUtil.active.value = false
                            }
                        }
                        false -> {
                            launch(Dispatchers.Main) {
                                InternetUtil.active.value = false
                                showRate.value = true
                            }
                        }
                        else -> {
                            InternetUtil.active.value = false
                        }
                    }
                }
            }
        }
    }

    fun panicButton() {
        uiScope.launch {
            WalkService.panicButton()
        }
    }
}