package com.petservices.screens.petwalk

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.petservices.data.models.Walk

class PetWalkViewModelFactory(val walk: Walk): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PetWalkViewModel(walk) as T
    }
}