package com.petservices.screens.userregister

import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.petservices.R
import com.petservices.databinding.RegisterFragmentBinding
import com.petservices.screens.login.LogInFragmentDirections

class RegisterFragment : Fragment() {

    private val viewModel: RegisterViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = RegisterFragmentBinding.inflate(inflater)
        val errorObserver = Observer<Int> { value ->
                when (value) {
                    1 -> showError(getString(R.string.check_email))
                    2 -> showError(getString(R.string.check_password))
                    3 -> showError(getString(R.string.check_confPassword))
                    4 -> showRegisterMessage()
                }
        }
        activity?.let { viewModel.messageType.observe(it, errorObserver) }
        activity?.let { viewModel.message.observe(it, { value ->
            if(value.isNotEmpty()&&value.isNotBlank()) {
                showError(value)
            }
        })}
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    private fun showError(message: String) {
        Snackbar.make(
            requireActivity().findViewById(android.R.id.content),
            message,
            Snackbar.LENGTH_LONG
        ).setDuration(3500).show()
    }

    private fun showRegisterMessage() {
        val alertDialog: AlertDialog? = activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setPositiveButton("OK",
                    DialogInterface.OnClickListener { dialog, id ->
                        val action = RegisterFragmentDirections.actionRegisterFragmentToLogInFragment()
                        findNavController().navigate(action)
                    })
            }
            // Create the AlertDialog
            builder.create()
        }
            alertDialog?.setTitle(this.resources.getString(R.string.register_successful_title))
            alertDialog?.setMessage(this.resources.getString(R.string.register_successful_user))
        alertDialog?.show()
    }
}