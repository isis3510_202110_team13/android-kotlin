package com.petservices.screens.userregister

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.petservices.data.services.PetOwnerService
import com.petservices.utils.InternetUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class RegisterViewModel : ViewModel() {
    val email = MutableLiveData("")
    val password = MutableLiveData("")
    val confirmPassword = MutableLiveData("")
    private val _messageType = MutableLiveData(0)

    val messageType: LiveData<Int>
        get() = _messageType

    private val _message = MutableLiveData("")

    val message: LiveData<String>
        get() = _message

    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    private val registerVM = Job()

    private val uiScope = CoroutineScope(Dispatchers.IO + registerVM)

    fun userRegister(view: View) {
        if (InternetUtil.isConnectedOrShowMessage()) {
            val emailClean = email.value ?: ""
            val passClean = password.value ?: ""
            val confPassClean = confirmPassword.value ?: ""
            var valid = true
            if (emailClean.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailClean)
                    .matches()
            ) {
                _messageType.value = 1
                valid = false
            } else if (passClean.length < 7) {
                _messageType.value = 2
                valid = false
            } else if (passClean != confPassClean) {
                _messageType.value = 3
                valid = false
            }
            if (valid) {
                InternetUtil.active.value = true
                firebaseAuth.createUserWithEmailAndPassword(emailClean, passClean)
                    .addOnSuccessListener {
                        uiScope.launch {
                            PetOwnerService.createPetOwner(it.user.uid)
                            launch(Dispatchers.Main) {
                                InternetUtil.active.value = false
                                _messageType.value = 4
                            }
                        }
                    }
                    .addOnFailureListener { error ->
                        InternetUtil.active.value = false
                        _message.value = error.message
                    }
            }
        }
    }
}