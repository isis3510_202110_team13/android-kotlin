package com.petservices.screens.walkshistory

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.petservices.data.models.Walk
import com.petservices.databinding.WalkItemBinding
import com.petservices.screens.walkshistory.OnClickListener

class WalksHistoryAdapter(private val onClickListener: OnClickListener): ListAdapter<Walk,WalksHistoryAdapter.ViewHolder >(WalkDiffCallback()) {

    class ViewHolder private constructor(private val binding: WalkItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(item: Walk) {
            binding.walk = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = WalkItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(item)
        }
        holder.bind(item)
    }
}

class WalkDiffCallback : DiffUtil.ItemCallback<Walk>() {

    override fun areItemsTheSame(oldItem: Walk, newItem: Walk): Boolean {
        return oldItem == newItem
    }


    override fun areContentsTheSame(oldItem: Walk, newItem: Walk): Boolean {
        return oldItem == newItem
    }
}

class OnClickListener(val clickListener: (walk:Walk) -> Unit) {
    fun onClick(walk:Walk) = clickListener(walk)
}