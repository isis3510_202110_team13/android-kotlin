package com.petservices.screens.walkshistory


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.*
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.*
import com.petservices.databinding.WalksHistoryFragmentBinding
import com.petservices.screens.walkshistory.WalksHistoryAdapter
import com.petservices.screens.walkshistory.OnClickListener

class WalksHistoryFragment : Fragment() {

    private lateinit var binding: WalksHistoryFragmentBinding
    private val viewModel: WalksHistoryViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = WalksHistoryFragmentBinding.inflate(inflater)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        val adapter = WalksHistoryAdapter(OnClickListener { this.findNavController().navigate(
            WalksHistoryFragmentDirections.actionWalksHistoryFragmentToPetWalkFragment(it)) })
        binding.walksHistoryList.adapter = adapter
        viewModel.getWalksV()
        return binding.root
    }
}