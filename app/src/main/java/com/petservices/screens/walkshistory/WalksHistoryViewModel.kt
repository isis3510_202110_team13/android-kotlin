package com.petservices.screens.walkshistory

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.petservices.data.entities.Pet
import com.petservices.data.entities.PetWalker
import com.petservices.data.entities.WalkE
import com.petservices.data.models.*
import com.petservices.data.services.WalkService
import com.petservices.utils.InternetUtil
import com.petservices.utils.RoomDB
import kotlinx.coroutines.*

class WalksHistoryViewModel : ViewModel() {

    private val walkVM = Job()

    private val uiScope = CoroutineScope(Dispatchers.IO + walkVM)

    private val _walks = MutableLiveData<List<Walk>>()

    val walks: LiveData<List<Walk>>
        get() = _walks

    init {
        if (InternetUtil.isConnectedOrShowMessage()) {
            InternetUtil.active.value = true
            getWalksV()
        } else {
            //getStoredWalks()
        }
    }

    fun getWalksV() {
        uiScope.launch {
            Log.d("WalksFragment", "launch -> ${Thread.currentThread().name}")
            try {
                var retrieved = WalkService.getWalks()
                var size = retrieved.size
                var w:Walk
                var datos:List<String>
                var fecha:List<String>
                var i = 0
                while(i<size)
                {
                    w = retrieved[i]
                    datos = w.startTime!!.split("T")
                    fecha = datos[0].split("-")
                    w.startTime = fecha[2] + "/" + fecha[1] + "/" + fecha[0].substring(2,4) + " at " +
                    datos[1].substring(0,5)
                    datos = w.endTime!!.split("T")
                    fecha = datos[0].split("-")
                    w.endTime = fecha[2] + "/" + fecha[1] + "/" + fecha[0].substring(2,4) + " at " +
                            datos[1].substring(0,5)
                    i++
                }
                launch(Dispatchers.Main) {
                    _walks.value = retrieved
                }
                Log.i("WalksViewModel", "Walks have been collected" + _walks.value.toString())
                var stored:MutableList<WalkE> = ArrayList()
                var wDAO = RoomDB.getDB().walkDAO()
                i = 0
                var walk:WalkE
                while(i<size)
                {
                    w = retrieved[i]
                    walk = WalkE(
                        w.id!!,w.startTime,w.endTime,w.status,w.rating,w.PetId,w.PetWalkerUid
                    )
                    stored.add(walk)
                    i++
                }
                wDAO.deleteAll()
                Log.i("WalksViewModel", "Walks have been registered" + stored.toString())
                //wDAO.insert(stored.toList())
                Log.i("WalksViewModel", "Walks have been roomed" + wDAO.getAll().toString())
            } catch (e: Exception) {
                launch(Dispatchers.Main) {
                    _walks.value = ArrayList()
                }
                Log.e("WalksViewModel", e.message?:"")
            }
            launch(Dispatchers.Main) {
                InternetUtil.active.value = false
            }
        }
    }

    private fun getStoredWalks() {
        uiScope.launch{
            try {
                var room = RoomDB.getDB()
                var saved = room.walkDAO().getAll()
                var pwDao = room.petWalkerDAO()
                var pDao = room.petDAO()
                var ws:MutableList<Walk> = ArrayList()
                var w:WalkE
                var pet:Pet
                var walk:Walk
                var pw:PetWalker
                var i = 0
                var size=saved.size
                while(i<size)
                {
                    w = saved[i]
                    pw = pwDao.getById(w.petWalkerId!!)
                    pet = pDao.getById(w.petId!!)
                    walk = Walk(w.id,w.startTime,w.endTime,w.status,w.rating, "","",w.petId!!,
                        w.petWalkerId!!, DogWalker(pw.id, pw.displayName, pw.photoUrl, pw.description, pw.avgRating,
                        FullLocation(CRS("name", Properties("EPSG:4326")),"Point",
                            listOf<Double>(pw.latitude!!.toDouble(),pw.longitude!!.toDouble())), pw.available, pw.city),
                        PetID(pet.id, pet.name,pet.breed, pet.age!!.toInt(),pet.size,pet.photoUrl, pet.petOwnerId)
                    )
                    ws.add(walk)
                    i++
                }
                launch(Dispatchers.Main) {
                    _walks.value = ws
                }
                Log.i("WalksViewModel", "Pet Walkers have been collected" + _walks.value.toString())
            } catch (e: Exception) {
                launch(Dispatchers.Main) {
                    _walks.value = ArrayList()
                }
                Log.e("WalksViewModel", e.message?:"")
            }
            launch(Dispatchers.Main) {
                InternetUtil.active.value = false
            }
        }
    }
}