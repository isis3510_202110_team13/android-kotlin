package com.petservices.utils

import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.petservices.R
import com.petservices.data.models.DogWalker
import com.petservices.data.models.Walk
import com.petservices.screens.dogwalkers.DogWalkersAdapter
import com.petservices.screens.walkshistory.WalksHistoryAdapter

/**
 * When there is no Mars property data (data is null), hide the [RecyclerView], otherwise show it.
 */
@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<DogWalker>?) {
    val adapter = recyclerView.adapter as DogWalkersAdapter
    adapter.submitList(data)
    recyclerView.smoothScrollToPosition(0)
}

@BindingAdapter("listData2")
fun bindRecyclerView2(recyclerView: RecyclerView, data: List<Walk>?) {
    val adapter = recyclerView.adapter as WalksHistoryAdapter
    adapter.submitList(data)
}

/**
 * Uses the Glide library to load an image by URL into an [ImageView]
 */
@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(imgView.context)
            .load(imgUri)
            .apply(
                RequestOptions()
                .placeholder(R.drawable.baseline_face_24)
                .error(R.drawable.baseline_broken_image_24))
            .into(imgView)
    }
}

/**
 * Uses the Glide library to load an image by URL into an [ImageView]
 */
@BindingAdapter("distance")
fun bindDistance(textView: TextView, distance: Double?) {
    if(distance == null || distance == 0.0){
        textView.text = "Unknown"
    }
    else{
        if(distance < 1000){
            textView.text = String.format("%.2f", distance) + " m"
        }else{
            textView.text = String.format("%.2f", distance/1000) + " km"
        }
    }
}