package com.petservices.utils

import android.app.Application
import com.petservices.data.DB

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        InternetUtil.init(this)
        RoomDB.init(DB.getInstance(this))
    }
}