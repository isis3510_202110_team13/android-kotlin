package com.petservices.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.petservices.R
import com.petservices.databinding.MainMenuFragmentBinding
import com.petservices.screens.mainmenu.MainMenuFragmentDirections


/**
 * A simple [Fragment] subclass.
 * Use the [NoInternetFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NoInternetFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_no_internet, container, false)
        val btn = view.findViewById<Button>(R.id.retryBtn)
            btn.setOnClickListener {
            if(InternetUtil.isConnected()){
                val action = NoInternetFragmentDirections.actionNoInternetFragmentToDogWalkersFragment()
                findNavController().navigate(action)
            }
        }
        return view
    }


    fun navigateToDogWalkers(){
        if(InternetUtil.isConnected()){
            val action = NoInternetFragmentDirections.actionNoInternetFragmentToDogWalkersFragment()
            findNavController().navigate(action)
        }
    }
}