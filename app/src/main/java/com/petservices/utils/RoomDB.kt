package com.petservices.utils

import com.petservices.data.DB

object RoomDB {
    private lateinit var rdb:DB

    fun init(rdb: DB) {
        this.rdb = rdb
    }

    fun getDB():DB{
        return rdb
    }
}